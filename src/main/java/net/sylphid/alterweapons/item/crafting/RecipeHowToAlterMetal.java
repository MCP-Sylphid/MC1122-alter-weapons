
package net.sylphid.alterweapons.item.crafting;

import net.sylphid.alterweapons.item.ItemAlterIron;
import net.sylphid.alterweapons.block.BlockAlterIronOre;
import net.sylphid.alterweapons.ElementsAlterWeaponsMod;

import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;

import net.minecraft.item.ItemStack;

@ElementsAlterWeaponsMod.ModElement.Tag
public class RecipeHowToAlterMetal extends ElementsAlterWeaponsMod.ModElement {
	public RecipeHowToAlterMetal(ElementsAlterWeaponsMod instance) {
		super(instance, 24);
	}

	@Override
	public void init(FMLInitializationEvent event) {
		GameRegistry.addSmelting(new ItemStack(BlockAlterIronOre.block, (int) (1)), new ItemStack(ItemAlterIron.block, (int) (1)), 1F);
	}
}
