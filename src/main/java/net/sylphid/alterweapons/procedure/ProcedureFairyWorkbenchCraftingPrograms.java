package net.sylphid.alterweapons.procedure;

import net.sylphid.alterweapons.item.ItemAlterTopazSword;
import net.sylphid.alterweapons.item.ItemAlterTopaz;
import net.sylphid.alterweapons.item.ItemAlterSapphireSword;
import net.sylphid.alterweapons.item.ItemAlterSapphire;
import net.sylphid.alterweapons.item.ItemAlterRubySword;
import net.sylphid.alterweapons.item.ItemAlterRuby;
import net.sylphid.alterweapons.item.ItemAlterIronSword;
import net.sylphid.alterweapons.item.ItemAlterEmeraldSword;
import net.sylphid.alterweapons.item.ItemAlterEmerald;
import net.sylphid.alterweapons.ElementsAlterWeaponsMod;

import net.minecraft.item.ItemStack;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.Container;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.Entity;

import java.util.function.Supplier;
import java.util.Map;

@ElementsAlterWeaponsMod.ModElement.Tag
public class ProcedureFairyWorkbenchCraftingPrograms extends ElementsAlterWeaponsMod.ModElement {
	public ProcedureFairyWorkbenchCraftingPrograms(ElementsAlterWeaponsMod instance) {
		super(instance, 43);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			System.err.println("Failed to load dependency entity for procedure FairyWorkbenchCraftingPrograms!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if ((((new Object() {
			public ItemStack getItemStack(int sltid) {
				if (entity instanceof EntityPlayerMP) {
					Container _current = ((EntityPlayerMP) entity).openContainer;
					if (_current instanceof Supplier) {
						Object invobj = ((Supplier) _current).get();
						if (invobj instanceof Map) {
							return ((Slot) ((Map) invobj).get(sltid)).getStack();
						}
					}
				}
				return ItemStack.EMPTY;
			}
		}.getItemStack((int) (110))).getItem() == new ItemStack(ItemAlterIronSword.block, (int) (1)).getItem()) && ((new Object() {
			public ItemStack getItemStack(int sltid) {
				if (entity instanceof EntityPlayerMP) {
					Container _current = ((EntityPlayerMP) entity).openContainer;
					if (_current instanceof Supplier) {
						Object invobj = ((Supplier) _current).get();
						if (invobj instanceof Map) {
							return ((Slot) ((Map) invobj).get(sltid)).getStack();
						}
					}
				}
				return ItemStack.EMPTY;
			}
		}.getItemStack((int) (111))).getItem() == new ItemStack(ItemAlterRuby.block, (int) (1)).getItem()))) {
			if (entity instanceof EntityPlayerMP) {
				Container _current = ((EntityPlayerMP) entity).openContainer;
				if (_current instanceof Supplier) {
					Object invobj = ((Supplier) _current).get();
					if (invobj instanceof Map) {
						ItemStack _setstack = new ItemStack(ItemAlterRubySword.block, (int) (1));
						_setstack.setCount(1);
						((Slot) ((Map) invobj).get((int) (112))).putStack(_setstack);
						_current.detectAndSendChanges();
					}
				}
			}
		} else if ((((new Object() {
			public ItemStack getItemStack(int sltid) {
				if (entity instanceof EntityPlayerMP) {
					Container _current = ((EntityPlayerMP) entity).openContainer;
					if (_current instanceof Supplier) {
						Object invobj = ((Supplier) _current).get();
						if (invobj instanceof Map) {
							return ((Slot) ((Map) invobj).get(sltid)).getStack();
						}
					}
				}
				return ItemStack.EMPTY;
			}
		}.getItemStack((int) (110))).getItem() == new ItemStack(ItemAlterIronSword.block, (int) (1)).getItem()) && ((new Object() {
			public ItemStack getItemStack(int sltid) {
				if (entity instanceof EntityPlayerMP) {
					Container _current = ((EntityPlayerMP) entity).openContainer;
					if (_current instanceof Supplier) {
						Object invobj = ((Supplier) _current).get();
						if (invobj instanceof Map) {
							return ((Slot) ((Map) invobj).get(sltid)).getStack();
						}
					}
				}
				return ItemStack.EMPTY;
			}
		}.getItemStack((int) (111))).getItem() == new ItemStack(ItemAlterSapphire.block, (int) (1)).getItem()))) {
			if (entity instanceof EntityPlayerMP) {
				Container _current = ((EntityPlayerMP) entity).openContainer;
				if (_current instanceof Supplier) {
					Object invobj = ((Supplier) _current).get();
					if (invobj instanceof Map) {
						ItemStack _setstack = new ItemStack(ItemAlterSapphireSword.block, (int) (1));
						_setstack.setCount(1);
						((Slot) ((Map) invobj).get((int) (112))).putStack(_setstack);
						_current.detectAndSendChanges();
					}
				}
			}
		} else if ((((new Object() {
			public ItemStack getItemStack(int sltid) {
				if (entity instanceof EntityPlayerMP) {
					Container _current = ((EntityPlayerMP) entity).openContainer;
					if (_current instanceof Supplier) {
						Object invobj = ((Supplier) _current).get();
						if (invobj instanceof Map) {
							return ((Slot) ((Map) invobj).get(sltid)).getStack();
						}
					}
				}
				return ItemStack.EMPTY;
			}
		}.getItemStack((int) (110))).getItem() == new ItemStack(ItemAlterIronSword.block, (int) (1)).getItem()) && ((new Object() {
			public ItemStack getItemStack(int sltid) {
				if (entity instanceof EntityPlayerMP) {
					Container _current = ((EntityPlayerMP) entity).openContainer;
					if (_current instanceof Supplier) {
						Object invobj = ((Supplier) _current).get();
						if (invobj instanceof Map) {
							return ((Slot) ((Map) invobj).get(sltid)).getStack();
						}
					}
				}
				return ItemStack.EMPTY;
			}
		}.getItemStack((int) (111))).getItem() == new ItemStack(ItemAlterTopaz.block, (int) (1)).getItem()))) {
			if (entity instanceof EntityPlayerMP) {
				Container _current = ((EntityPlayerMP) entity).openContainer;
				if (_current instanceof Supplier) {
					Object invobj = ((Supplier) _current).get();
					if (invobj instanceof Map) {
						ItemStack _setstack = new ItemStack(ItemAlterTopazSword.block, (int) (1));
						_setstack.setCount(1);
						((Slot) ((Map) invobj).get((int) (112))).putStack(_setstack);
						_current.detectAndSendChanges();
					}
				}
			}
		} else if ((((new Object() {
			public ItemStack getItemStack(int sltid) {
				if (entity instanceof EntityPlayerMP) {
					Container _current = ((EntityPlayerMP) entity).openContainer;
					if (_current instanceof Supplier) {
						Object invobj = ((Supplier) _current).get();
						if (invobj instanceof Map) {
							return ((Slot) ((Map) invobj).get(sltid)).getStack();
						}
					}
				}
				return ItemStack.EMPTY;
			}
		}.getItemStack((int) (110))).getItem() == new ItemStack(ItemAlterIronSword.block, (int) (1)).getItem()) && ((new Object() {
			public ItemStack getItemStack(int sltid) {
				if (entity instanceof EntityPlayerMP) {
					Container _current = ((EntityPlayerMP) entity).openContainer;
					if (_current instanceof Supplier) {
						Object invobj = ((Supplier) _current).get();
						if (invobj instanceof Map) {
							return ((Slot) ((Map) invobj).get(sltid)).getStack();
						}
					}
				}
				return ItemStack.EMPTY;
			}
		}.getItemStack((int) (111))).getItem() == new ItemStack(ItemAlterEmerald.block, (int) (1)).getItem()))) {
			if (entity instanceof EntityPlayerMP) {
				Container _current = ((EntityPlayerMP) entity).openContainer;
				if (_current instanceof Supplier) {
					Object invobj = ((Supplier) _current).get();
					if (invobj instanceof Map) {
						ItemStack _setstack = new ItemStack(ItemAlterEmeraldSword.block, (int) (1));
						_setstack.setCount(1);
						((Slot) ((Map) invobj).get((int) (112))).putStack(_setstack);
						_current.detectAndSendChanges();
					}
				}
			}
		}
	}
}
