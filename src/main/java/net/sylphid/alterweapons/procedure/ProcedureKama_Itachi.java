package net.sylphid.alterweapons.procedure;

import net.sylphid.alterweapons.ElementsAlterWeaponsMod;

import net.minecraft.world.World;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.DamageSource;
import net.minecraft.entity.Entity;

import java.util.Map;

@ElementsAlterWeaponsMod.ModElement.Tag
public class ProcedureKama_Itachi extends ElementsAlterWeaponsMod.ModElement {
	public ProcedureKama_Itachi(ElementsAlterWeaponsMod instance) {
		super(instance, 55);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			System.err.println("Failed to load dependency entity for procedure Kama_Itachi!");
			return;
		}
		if (dependencies.get("x") == null) {
			System.err.println("Failed to load dependency x for procedure Kama_Itachi!");
			return;
		}
		if (dependencies.get("y") == null) {
			System.err.println("Failed to load dependency y for procedure Kama_Itachi!");
			return;
		}
		if (dependencies.get("z") == null) {
			System.err.println("Failed to load dependency z for procedure Kama_Itachi!");
			return;
		}
		if (dependencies.get("world") == null) {
			System.err.println("Failed to load dependency world for procedure Kama_Itachi!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		int x = (int) dependencies.get("x");
		int y = (int) dependencies.get("y");
		int z = (int) dependencies.get("z");
		World world = (World) dependencies.get("world");
		for (int index0 = 0; index0 < (int) (10); index0++) {
			world.spawnParticle(EnumParticleTypes.EXPLOSION_NORMAL, x, y, z, 0, 1, 0);
			entity.attackEntityFrom(DamageSource.GENERIC, (float) 1);
		}
	}
}
