package net.sylphid.alterweapons.procedure;

import net.sylphid.alterweapons.ElementsAlterWeaponsMod;

import net.minecraft.world.World;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.init.Blocks;

import java.util.Map;

@ElementsAlterWeaponsMod.ModElement.Tag
public class ProcedureBlastBlaze extends ElementsAlterWeaponsMod.ModElement {
	public ProcedureBlastBlaze(ElementsAlterWeaponsMod instance) {
		super(instance, 47);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("direction") == null) {
			System.err.println("Failed to load dependency direction for procedure BlastBlaze!");
			return;
		}
		if (dependencies.get("x") == null) {
			System.err.println("Failed to load dependency x for procedure BlastBlaze!");
			return;
		}
		if (dependencies.get("y") == null) {
			System.err.println("Failed to load dependency y for procedure BlastBlaze!");
			return;
		}
		if (dependencies.get("z") == null) {
			System.err.println("Failed to load dependency z for procedure BlastBlaze!");
			return;
		}
		if (dependencies.get("world") == null) {
			System.err.println("Failed to load dependency world for procedure BlastBlaze!");
			return;
		}
		EnumFacing direction = (EnumFacing) dependencies.get("direction");
		int x = (int) dependencies.get("x");
		int y = (int) dependencies.get("y");
		int z = (int) dependencies.get("z");
		World world = (World) dependencies.get("world");
		double MainLoop = 0;
		double SubLoop = 0;
		double local_X = 0;
		double local_Z = 0;
		MainLoop = (double) 0;
		SubLoop = (double) 1;
		while (((MainLoop) < 5)) {
			MainLoop = (double) ((MainLoop) + 1);
			for (int index1 = 0; index1 < (int) ((SubLoop)); index1++) {
				if ((direction == EnumFacing.NORTH)) {
					local_X = (double) (x - ((SubLoop) - 1));
					local_Z = (double) (z + (MainLoop));
					if ((world.isAirBlock(new BlockPos((int) (local_X), (int) (y + 1), (int) (local_Z))))) {
						world.setBlockState(new BlockPos((int) (local_X), (int) (y + 1), (int) (local_Z)), Blocks.FIRE.getDefaultState(), 3);
					}
				}
			}
			SubLoop = (double) ((SubLoop) + 2);
		}
	}
}
