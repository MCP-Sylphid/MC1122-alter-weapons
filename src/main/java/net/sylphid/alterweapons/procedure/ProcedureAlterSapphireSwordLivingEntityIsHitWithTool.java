package net.sylphid.alterweapons.procedure;

import net.sylphid.alterweapons.potion.PotionFrost;
import net.sylphid.alterweapons.ElementsAlterWeaponsMod;

import net.minecraft.potion.PotionEffect;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.Entity;

import java.util.Map;

@ElementsAlterWeaponsMod.ModElement.Tag
public class ProcedureAlterSapphireSwordLivingEntityIsHitWithTool extends ElementsAlterWeaponsMod.ModElement {
	public ProcedureAlterSapphireSwordLivingEntityIsHitWithTool(ElementsAlterWeaponsMod instance) {
		super(instance, 38);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			System.err.println("Failed to load dependency entity for procedure AlterSapphireSwordLivingEntityIsHitWithTool!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if (entity instanceof EntityLivingBase)
			((EntityLivingBase) entity).addPotionEffect(new PotionEffect(PotionFrost.potion, (int) 180, (int) 5));
	}
}
