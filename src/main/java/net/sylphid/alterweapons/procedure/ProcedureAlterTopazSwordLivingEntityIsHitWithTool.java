package net.sylphid.alterweapons.procedure;

import net.sylphid.alterweapons.ElementsAlterWeaponsMod;

import net.minecraft.world.World;
import net.minecraft.entity.effect.EntityLightningBolt;

import java.util.Map;

@ElementsAlterWeaponsMod.ModElement.Tag
public class ProcedureAlterTopazSwordLivingEntityIsHitWithTool extends ElementsAlterWeaponsMod.ModElement {
	public ProcedureAlterTopazSwordLivingEntityIsHitWithTool(ElementsAlterWeaponsMod instance) {
		super(instance, 26);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("x") == null) {
			System.err.println("Failed to load dependency x for procedure AlterTopazSwordLivingEntityIsHitWithTool!");
			return;
		}
		if (dependencies.get("y") == null) {
			System.err.println("Failed to load dependency y for procedure AlterTopazSwordLivingEntityIsHitWithTool!");
			return;
		}
		if (dependencies.get("z") == null) {
			System.err.println("Failed to load dependency z for procedure AlterTopazSwordLivingEntityIsHitWithTool!");
			return;
		}
		if (dependencies.get("world") == null) {
			System.err.println("Failed to load dependency world for procedure AlterTopazSwordLivingEntityIsHitWithTool!");
			return;
		}
		int x = (int) dependencies.get("x");
		int y = (int) dependencies.get("y");
		int z = (int) dependencies.get("z");
		World world = (World) dependencies.get("world");
		double host_x = 0;
		double host_z = 0;
		double flame_LoopTime = 0;
		host_x = (double) (x - 1);
		host_z = (double) (z - 1);
		flame_LoopTime = (double) 1;
		world.addWeatherEffect(new EntityLightningBolt(world, (int) x, (int) y, (int) z, false));
	}
}
