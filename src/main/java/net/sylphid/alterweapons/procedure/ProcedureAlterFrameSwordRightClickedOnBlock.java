package net.sylphid.alterweapons.procedure;

import net.sylphid.alterweapons.ElementsAlterWeaponsMod;

import net.minecraft.world.WorldServer;
import net.minecraft.world.World;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.init.Blocks;

import java.util.Map;

@ElementsAlterWeaponsMod.ModElement.Tag
public class ProcedureAlterFrameSwordRightClickedOnBlock extends ElementsAlterWeaponsMod.ModElement {
	public ProcedureAlterFrameSwordRightClickedOnBlock(ElementsAlterWeaponsMod instance) {
		super(instance, 30);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("x") == null) {
			System.err.println("Failed to load dependency x for procedure AlterFrameSwordRightClickedOnBlock!");
			return;
		}
		if (dependencies.get("y") == null) {
			System.err.println("Failed to load dependency y for procedure AlterFrameSwordRightClickedOnBlock!");
			return;
		}
		if (dependencies.get("z") == null) {
			System.err.println("Failed to load dependency z for procedure AlterFrameSwordRightClickedOnBlock!");
			return;
		}
		if (dependencies.get("world") == null) {
			System.err.println("Failed to load dependency world for procedure AlterFrameSwordRightClickedOnBlock!");
			return;
		}
		int x = (int) dependencies.get("x");
		int y = (int) dependencies.get("y");
		int z = (int) dependencies.get("z");
		World world = (World) dependencies.get("world");
		double LoopTime = 0;
		double Host_X = 0;
		double Host_Z = 0;
		double NestedLoopTime = 0;
		double NestNestedLoopTime = 0;
		if (world instanceof WorldServer)
			((WorldServer) world).spawnParticle(EnumParticleTypes.EXPLOSION_HUGE, x, y, z, (int) 5, 5, 5, 5, 10, new int[0]);
		LoopTime = (double) 0;
		NestedLoopTime = (double) (-1);
		while (((LoopTime) < 5)) {
			NestedLoopTime = (double) ((NestedLoopTime) + 2);
			NestNestedLoopTime = (double) 0;
			if ((true)) {
				Host_X = (double) (x + 1);
				for (int index1 = 0; index1 < (int) ((NestedLoopTime)); index1++) {
					world.setBlockState(new BlockPos((int) ((Host_X) - (NestNestedLoopTime)), (int) (y + 1), (int) (z + ((LoopTime) + 1))),
							Blocks.FIRE.getDefaultState(), 3);
					NestNestedLoopTime = (double) ((NestNestedLoopTime) + 1);
				}
			} else if ((true)) {
				Host_X = (double) (x - 1);
				for (int index2 = 0; index2 < (int) ((NestedLoopTime)); index2++) {
					world.setBlockState(new BlockPos((int) ((Host_X) + (NestNestedLoopTime)), (int) (y + 1), (int) (z - ((LoopTime) + 1))),
							Blocks.FIRE.getDefaultState(), 3);
					NestNestedLoopTime = (double) ((NestNestedLoopTime) + 1);
				}
			} else if ((true)) {
				Host_Z = (double) (z + 1);
				for (int index3 = 0; index3 < (int) ((NestedLoopTime)); index3++) {
					world.setBlockState(new BlockPos((int) (x - ((LoopTime) + 1)), (int) (y + 1), (int) ((Host_Z) + (NestNestedLoopTime))),
							Blocks.FIRE.getDefaultState(), 3);
					NestNestedLoopTime = (double) ((NestNestedLoopTime) + 1);
				}
			} else if ((true)) {
				Host_Z = (double) (z - 1);
				for (int index4 = 0; index4 < (int) ((NestedLoopTime)); index4++) {
					world.setBlockState(new BlockPos((int) (x + ((LoopTime) + 1)), (int) (y + 1), (int) ((Host_Z) - (NestNestedLoopTime))),
							Blocks.FIRE.getDefaultState(), 3);
					NestNestedLoopTime = (double) ((NestNestedLoopTime) + 1);
				}
			}
			LoopTime = (double) ((LoopTime) + 1);
		}
	}
}
