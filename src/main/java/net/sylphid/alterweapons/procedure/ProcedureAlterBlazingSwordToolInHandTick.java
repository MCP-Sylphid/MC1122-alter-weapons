package net.sylphid.alterweapons.procedure;

import net.sylphid.alterweapons.ElementsAlterWeaponsMod;

import net.minecraft.potion.PotionEffect;
import net.minecraft.init.MobEffects;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.Entity;

import java.util.Map;

@ElementsAlterWeaponsMod.ModElement.Tag
public class ProcedureAlterBlazingSwordToolInHandTick extends ElementsAlterWeaponsMod.ModElement {
	public ProcedureAlterBlazingSwordToolInHandTick(ElementsAlterWeaponsMod instance) {
		super(instance, 51);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			System.err.println("Failed to load dependency entity for procedure AlterBlazingSwordToolInHandTick!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if (entity instanceof EntityLivingBase)
			((EntityLivingBase) entity).addPotionEffect(new PotionEffect(MobEffects.FIRE_RESISTANCE, (int) 1, (int) 1, (true), (true)));
	}
}
