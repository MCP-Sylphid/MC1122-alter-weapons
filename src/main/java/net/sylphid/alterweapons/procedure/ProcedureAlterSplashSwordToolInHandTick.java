package net.sylphid.alterweapons.procedure;

import net.sylphid.alterweapons.ElementsAlterWeaponsMod;

import net.minecraft.potion.PotionEffect;
import net.minecraft.init.MobEffects;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.Entity;

import java.util.Map;

@ElementsAlterWeaponsMod.ModElement.Tag
public class ProcedureAlterSplashSwordToolInHandTick extends ElementsAlterWeaponsMod.ModElement {
	public ProcedureAlterSplashSwordToolInHandTick(ElementsAlterWeaponsMod instance) {
		super(instance, 56);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			System.err.println("Failed to load dependency entity for procedure AlterSplashSwordToolInHandTick!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if ((entity.isBurning())) {
			(entity).extinguish();
		}
		if (entity instanceof EntityLivingBase)
			((EntityLivingBase) entity).addPotionEffect(new PotionEffect(MobEffects.WATER_BREATHING, (int) 1, (int) 1));
	}
}
