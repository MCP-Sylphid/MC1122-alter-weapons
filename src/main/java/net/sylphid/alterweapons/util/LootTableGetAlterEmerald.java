
package net.sylphid.alterweapons.util;

import net.sylphid.alterweapons.ElementsAlterWeaponsMod;

import net.minecraftforge.fml.common.event.FMLInitializationEvent;

import net.minecraft.world.storage.loot.LootTableList;
import net.minecraft.util.ResourceLocation;

@ElementsAlterWeaponsMod.ModElement.Tag
public class LootTableGetAlterEmerald extends ElementsAlterWeaponsMod.ModElement {
	public LootTableGetAlterEmerald(ElementsAlterWeaponsMod instance) {
		super(instance, 46);
	}

	@Override
	public void init(FMLInitializationEvent event) {
		LootTableList.register(new ResourceLocation("alter_weapons", "blocks/alter_emerald"));
	}
}
