
package net.sylphid.alterweapons.util;

import net.sylphid.alterweapons.ElementsAlterWeaponsMod;

import net.minecraftforge.fml.common.event.FMLInitializationEvent;

import net.minecraft.world.storage.loot.LootTableList;
import net.minecraft.util.ResourceLocation;

@ElementsAlterWeaponsMod.ModElement.Tag
public class LootTableGetAlterSaphhire extends ElementsAlterWeaponsMod.ModElement {
	public LootTableGetAlterSaphhire(ElementsAlterWeaponsMod instance) {
		super(instance, 28);
	}

	@Override
	public void init(FMLInitializationEvent event) {
		LootTableList.register(new ResourceLocation("alter_weapons", "blocks/alter_sapphire"));
	}
}
