
package net.sylphid.alterweapons.creativetab;

import net.sylphid.alterweapons.block.BlockAlterIronOre;
import net.sylphid.alterweapons.ElementsAlterWeaponsMod;

import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;

import net.minecraft.item.ItemStack;
import net.minecraft.creativetab.CreativeTabs;

@ElementsAlterWeaponsMod.ModElement.Tag
public class TabAlterWeaponsBlocks extends ElementsAlterWeaponsMod.ModElement {
	public TabAlterWeaponsBlocks(ElementsAlterWeaponsMod instance) {
		super(instance, 32);
	}

	@Override
	public void initElements() {
		tab = new CreativeTabs("tabalter_weapons_blocks") {
			@SideOnly(Side.CLIENT)
			@Override
			public ItemStack getTabIconItem() {
				return new ItemStack(BlockAlterIronOre.block, (int) (1));
			}

			@SideOnly(Side.CLIENT)
			public boolean hasSearchBar() {
				return false;
			}
		};
	}
	public static CreativeTabs tab;
}
