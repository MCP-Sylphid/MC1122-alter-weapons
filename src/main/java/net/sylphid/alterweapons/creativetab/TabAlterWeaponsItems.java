
package net.sylphid.alterweapons.creativetab;

import net.sylphid.alterweapons.item.ItemAlterIron;
import net.sylphid.alterweapons.ElementsAlterWeaponsMod;

import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;

import net.minecraft.item.ItemStack;
import net.minecraft.creativetab.CreativeTabs;

@ElementsAlterWeaponsMod.ModElement.Tag
public class TabAlterWeaponsItems extends ElementsAlterWeaponsMod.ModElement {
	public TabAlterWeaponsItems(ElementsAlterWeaponsMod instance) {
		super(instance, 33);
	}

	@Override
	public void initElements() {
		tab = new CreativeTabs("tabalter_weapons_items") {
			@SideOnly(Side.CLIENT)
			@Override
			public ItemStack getTabIconItem() {
				return new ItemStack(ItemAlterIron.block, (int) (1));
			}

			@SideOnly(Side.CLIENT)
			public boolean hasSearchBar() {
				return false;
			}
		};
	}
	public static CreativeTabs tab;
}
