
package net.sylphid.alterweapons.creativetab;

import net.sylphid.alterweapons.item.ItemAlterIronSword;
import net.sylphid.alterweapons.ElementsAlterWeaponsMod;

import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;

import net.minecraft.item.ItemStack;
import net.minecraft.creativetab.CreativeTabs;

@ElementsAlterWeaponsMod.ModElement.Tag
public class TabAlterWeaponsWeapons extends ElementsAlterWeaponsMod.ModElement {
	public TabAlterWeaponsWeapons(ElementsAlterWeaponsMod instance) {
		super(instance, 34);
	}

	@Override
	public void initElements() {
		tab = new CreativeTabs("tabalter_weapons_weapons") {
			@SideOnly(Side.CLIENT)
			@Override
			public ItemStack getTabIconItem() {
				return new ItemStack(ItemAlterIronSword.block, (int) (1));
			}

			@SideOnly(Side.CLIENT)
			public boolean hasSearchBar() {
				return false;
			}
		};
	}
	public static CreativeTabs tab;
}
